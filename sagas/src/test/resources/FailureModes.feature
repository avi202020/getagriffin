@Failure
@Saga
Feature: FailureModes

	Scenario: FulfillGriffin update succeeds but FulfillSaddle update fails

		Given Order makes a request to FulfillGriffin
		And the FulfillGriffin request succeeds

		When Order immediately makes a request to FulfillSaddle
		And the FulfillSaddle request fails

		Then Order reverses the FulfillGriffin request
		And Order returns an error response
