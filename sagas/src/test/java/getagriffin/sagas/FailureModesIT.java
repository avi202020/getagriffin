package getagriffin.sagas;

import fulfillgriffin.stub.*;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;

public class FailureModesIT {



	public FailureModesIT() {
	}

	/* Scenario: FulfillGriffin update succeeds but FulfillSaddle update fails */

	@Given("Order makes a request to FulfillGriffin")
	public void order_makes_request_to_fulfillgriffin() throws Exception {
		throw new Exception("Not implemented yet");
	}

	@Given("the FulfillGriffin request succeeds")
	public void fulfillgriffin_request_succeeds() throws Exception {
		throw new Exception("Not implemented yet");
	}

	@When("Order immediately makes a request to FulfillSaddle")
	public void order_makes_request_to_fulfillsaddle() throws Exception {
		throw new Exception("Not implemented yet");
	}

	@When("the FulfillSaddle request fails")
	public void fulfillsaddle_request_fails() throws Exception {
		throw new Exception("Not implemented yet");
	}

	@Then("Order reverses the FulfillGriffin request")
	public void order_reverses_fulfullgriffin_request() throws Exception {
		throw new Exception("Not implemented yet");
	}

	@Then("Order returns an error response")
	public void order_returns_error() throws Exception {
		throw new Exception("Not implemented yet");
	}
}
