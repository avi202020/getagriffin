package getagriffin.failuremodes;

import fulfillgriffin.stub.*;
import getagriffin.container.ContainerProvider;
import getagriffin.container.ComposeContainerProvider;
import fulfillsaddle.stub.FulfillSaddleFactory;
import fulfillsaddle.stub.FulfillSaddleStub;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;

public class FailureModesIT {

	private String[] containerIds;
	private Exception fulfillsaddleException = null;
	private ContainerProvider containerProvider = null;
	private String fulfillSaddleFactoryClassName = System.getenv("FULFILL_SADDLE_FACTORY_CLASS_NAME");
	private FulfillSaddleFactory fsFac;
	private FulfillSaddleStub fs;

	public FailureModesIT() {
		this.containerProvider = new ComposeContainerProvider();
		if (fulfillSaddleFactoryClassName == null) throw new Error(
			"Env variable FULFILL_SADDLE_FACTORY_CLASS_NAME not set");
		try {
			fsFac = (FulfillSaddleFactory)(Class.forName(fulfillSaddleFactoryClassName).getDeclaredConstructor().newInstance());
			fs = fsFac.createFulfillSaddle();
		} catch (Exception ex) {
			throw new RuntimeException("During startup", ex);
		}
		System.err.println("Constructed FulfillSaddle stub");
	}


	/* Scenario: FulfillSaddle hangs */

	@Given("that FulfillSaddle is scaled to {int} container")
	public void fulfillsaddle_is_scaled_to(int nContainers) throws Exception {
		/* Obtain the IDs of the running instances of the fulfillsaddle container: */
		this.containerIds = getFulfillSaddleContainerIds();
		assertEquals("Number of fulfillsaddle instances is not " + nContainers,
			containerIds.length, nContainers);
	}

	@Given("FulfillSaddle has hung")
	public void fulfillsaddle_has_hung() throws Exception {
		/* Stop the container: */
		stopContainer(this.containerIds[0]);
	}

	@Given("at least {int} milliseconds have passed since it hung")
	public void at_least_n_milliseconds_have_passed(int ms) throws Exception {
		waitFor(ms);
	}

	@When("I access FulfillSaddle")
	public void access_fulfill_saddle() throws Exception {
		/* Call the FulfillSaddle service: */
		try {
			String response = fs.ping();
			assertEquals("Response was not ping: it was " + response,
				response, "ping");
		} catch (Exception ex) {
			this.fulfillsaddleException = ex;
		}
	}

	@Then("it has recovered and I receive a non-error result")
	public void received_non_error_result_from_fulfillsaddle() throws Exception {
		assertNull(this.fulfillsaddleException);
	}

	/* Internal methods */

	/** Obtain an array of the container Ids of the running instances of FulfillSaddle. */
	private String[] getFulfillSaddleContainerIds() throws Exception {
		return containerProvider.getContainerIds("FulfillSaddle");
	}

	/** Cause the container with the specified ID to stop running. */
	private void stopContainer(String containerId) throws Exception {
		containerProvider.stopContainer(containerId);
	}

	/** Cause the current thread to wait for the specified number of milliseconds. */
	private void waitFor(int ms) throws Exception {
		Thread.currentThread().sleep(ms);
	}
}
